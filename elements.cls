\ProvidesClass{elements}[2015/10/12 M. T. McCarthy Template based on Euclid's Elements]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions \relax

\LoadClass{report}

\renewcommand{\chaptername}{Book}
%\renewcommand{\thechapter}{}
\def\book{\chapter{}}

\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{float}
\RequirePackage{fullpage}
\RequirePackage{graphicx}
\RequirePackage{hyperref}
\RequirePackage{mathtools}
\RequirePackage{setspace}
\RequirePackage{url}

\RequirePackage[backend=bibtex, style=numeric]{biblatex}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[chapter]
\newcommand{\thmautorefname}{Theorem}
\newtheorem{corollary}[thm]{Corollary}
\newcommand{\corollaryautorefname}{Corrollary}
\newtheorem{lemma}[thm]{Lemma}
\newcommand{\lemmaautorefname}{Lemma}
\newtheorem{example}{Example}[chapter]
\newcommand{\exampleautorefname}{Example}
\newtheorem{proposition}{Proposition}[chapter]
\newcommand{\propositionautorefname}{Proposition}
\newtheorem{notion}{Common Notion}[chapter]
\newcommand{\notionautorefname}{Common Notion}
\newtheorem{property}[thm]{Property}
\newcommand{\propertyautorefname}{Property}
\newtheorem{postulate}{Postulate}[chapter]
\newcommand{\postulateautorefname}{Postulate}
\newtheorem*{thm*}{Theorem}
\newtheorem*{corollary*}{Corollary}
\newtheorem*{lemma*}{Lemma}
\newtheorem*{example*}{Example}
\newtheorem*{proposition*}{Proposition}
\newtheorem*{property*}{Property}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[chapter]
\newcommand{\definitionautorefname}{Definition}
\newtheorem*{definition*}{Definition}

\def\AA{\mathbb{A}}	% Algebraics
\def\CC{\mathbb{C}}	% Complex
\def\FF{\mathbb{F}}	% Arbitrary field
\def\HH{\mathbb{H}}	% Quaternions
\def\KK{\mathbb{K}}	% Arbitrary field
\def\NN{\mathbb{N}}	% Naturals
\def\QQ{\mathbb{Q}}	% Rationals
\def\RR{\mathbb{R}}	% Reals
\def\ZZ{\mathbb{Z}}	% Integers

\renewcommand\Re{\operatorname{Re}}	% Real part of a complex number
\renewcommand\Im{\operatorname{Im}}	% Imaginary part of a complex number
\newcommand{\charec}{\operatorname{char}}

\newcommand{\ceil}[1]{\lceil #1 \rceil}
\newcommand{\floor}[1]{\lfloor #1 \rfloor}
\newcommand{\set}[1]{\lbrace #1 \rbrace}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\brac}[1]{\left[ #1 \right]}
\newcommand{\angl}[1]{\langle #1 \rangle}
\newcommand{\abs}[1]{\left| #1 \right|}
