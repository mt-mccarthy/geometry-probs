\documentclass[notitlepage]{elements}

% Begin Extra includes



% End Extra includes

% Begin Tikz includes

\usepackage{pgf,tikz}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\newcommand{\degre}{\ensuremath{^\circ}}

\definecolor{qqwuqq}{rgb}{0.,0.39215686274509803,0.}
\definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
\definecolor{uuuuuu}{rgb}{0.26666666666666666,0.26666666666666666,0.26666666666666666}
\definecolor{zzttqq}{rgb}{0.6,0.2,0.}
\definecolor{qqqqff}{rgb}{0.,0.,1.}

% End Tikz includes

\title{Matt's Geometry Problems...\\ With Solutions Shamelessly Stolen from Euclid}
\date{Spring 2016}
\author{Matt McCarthy}

% \addbibresource{biblio.bib}

\newcommand{\ma}{\measuredangle}
\newcommand{\area}[1]{area\paren{#1}}

\begin{document}

\maketitle

\book

\section{Definitions}

\begin{definition}
	A \textit{point} is that which has no part.
\end{definition}

\begin{definition}
	A \textit{line} is breadthless length.
\end{definition}

\begin{definition}
	The ends of a line are points.
\end{definition}

\begin{definition}
	A \textit{straight line} is a line which lies evenly with the points on itself.
\end{definition}

\begin{definition}
	A \textit{surface} is that which has length and breadth only.
\end{definition}

\begin{definition}
	The edges of a surface are lines.
\end{definition}

\begin{definition}
	A \textit{plane surface} is a surface which lies evenly with the straight lines on itself.
\end{definition}

\begin{definition}
	A \textit{plane angle} is the inclination to one another of two lines in a plane which meet one another and do not lie in a straight line.
\end{definition}

\begin{definition}
	And when the lines containing the angle are straight, the angle is called \textit{rectilinear}.
\end{definition}

\begin{definition}
	When a straight line standing on a straight line makes the adjacent angles equal to one another,
	each of the equal angles is \textit{right},
	and the straight line standing on the other is called a \textit{perpendicular} to that on which it stands.
\end{definition}

\begin{definition}
	An \textit{obtuse angle} is an angle greater than a right angle.
\end{definition}

\begin{definition}
	An \textit{acute angle} is an angle less than a right angle.
\end{definition}

\begin{definition}
	A \textit{boundary} is that which is an extremity of anything.
\end{definition}

\begin{definition}
	A \textit{figure} is that which is contained by any boundary or boundaries.
\end{definition}

\begin{definition}
	A \textit{circle} is a plane figure contained by one line such that all the straight lines falling upon it from one point among those lying within the figure equal one another.
\end{definition}

\begin{definition}
	And the point is called the \textit{center of the circle}.
\end{definition}

\begin{definition}
	A \textit{diameter} of the circle is any straight line drawn through the center and terminated in both directions by the circumference of the circle,
	and such a straight line also bisects the circle.
\end{definition}

\begin{definition}
	A \textit{semicircle} is the figure contained by the diameter and the circumference cut off by it.
	And the center of the semicircle is the same as that of the circle.
\end{definition}

\begin{definition}
	Rectilinear figures are those which are contained by straight lines, trilateral figures being those contained by three, quadrilateral those contained by four, and multilateral those contained by more than four straight lines.
\end{definition}

\begin{definition}
	Of trilateral figures, an \textit{equilateral triangle} is that which has its three sides equal, an \textit{isosceles triangle} that which has two of its sides alone equal, and a \textit{scalene triangle} that which has its three sides unequal.
\end{definition}

\begin{definition}
	Further, of trilateral figures, a \textit{right-angled triangle} is that which has a right angle, an \textit{obtuse-angled triangle} that which has an obtuse angle, and an \textit{acute-angled triangle} that which has its three angles acute.
\end{definition}

\begin{definition}
	Of quadrilateral figures, a \textit{square} is that which is both equilateral and right-angled;
	an \textit{oblong} that which is right-angled but not equilateral;
	a \textit{rhombus} that which is equilateral but not right-angled;
	and a \textit{rhomboid} that which has its opposite sides and angles equal to one another but is neither equilateral nor right-angled.
	And let quadrilaterals other than these be called \textit{trapezia}.
\end{definition}

\begin{definition}
	\textit{Parallel} straight lines are straight lines which, being in the same plane and being produced indefinitely in both directions, do not meet one another in either direction.
\end{definition}

\section{Postulates}

\begin{postulate}
	To draw a straight line from any point to any point.
\end{postulate}

\begin{postulate}
	To produce a finite straight line continuously in a straight line.
\end{postulate}

\begin{postulate}
	To describe a circle with any center and radius.
\end{postulate}

\begin{postulate}
	That all right angles equal one another.
\end{postulate}

\begin{postulate}
	That, if a straight line falling on two straight lines makes the interior angles on the same side less than two right angles, the two straight lines, if produced indefinitely, meet on that side on which are the angles less than the two right angles.
\end{postulate}

\section{Common Notions}

\begin{notion}
	Things which equal the same thing also equal one another.
\end{notion}

\begin{notion}
	If equals are added to equals, then the wholes are equal.
\end{notion}

\begin{notion}
	If equals are subtracted from equals, then the remainders are equal.
\end{notion}

\begin{notion}
	Things which coincide with one another equal one another.
\end{notion}

\begin{notion}
	The whole is greater than the part.
\end{notion}

\section{Propositions}

\setcounter{proposition}{45}
\begin{proposition}\label{b1pr46}
	Given a straight line $AB$, we can construct a square of side length $AB$.
\end{proposition}
\begin{proof}
	Consider $AB$.
	By Proposition 1.11, we can construct a straight line $AC$ that is orthogonal to $AB$.
	Let $D$ be on $AC$ such that $AD=AB$.
	By Proposition 1.31 we can find a point $E$ such that $DE$ is parallel to $AB$ and $BE$ is parallel to $AD$.

	Thus, $ABED$ is a parallelogram and by Proposition 1.34 $AB=DE$ and $AD=BE$.
	However, since $AB=AD$, $DE=BE$ and $ABED$ is equilateral.
	Since $AD$ transverses the parallel lines $AB$ and $DE$, $\ma BAC +\ma ADE = 180$.
	However, since $\ma BAC = 90$, $\ma ADE=90$.
	Moreover, since $ABED$ is a parallelogram, $\ma DEB =\ma BAC =90$ and $\ma EBA=\ma ADE=90$ by Proposition 1.34.
	Thus, $ABED$ is a square.

	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(0.5,0.5) rectangle (4.5,5.5);
		\draw[color=qqwuqq,fill=qqwuqq,fill opacity=0.1] (1.4242640687119288,1.) -- (1.4242640687119288,1.4242640687119286) -- (1.,1.4242640687119288) -- (1.,1.) -- cycle;
		\draw (1.,1.)-- (4.,1.);
		\draw (1.,1.)-- (1.,5.);
		\draw (1.,1.)-- (1.,4.);
		\draw (4.,1.)-- (4.,4.);
		\draw (1.,4.)-- (4.,4.);
		\begin{scriptsize}
		\draw [fill=qqqqff] (1.,1.) circle (2.5pt);
		\draw[color=qqqqff] (0.68,1.16) node {$A$};
		\draw [fill=qqqqff] (4.,1.) circle (2.5pt);
		\draw[color=qqqqff] (4.14,1.36) node {$B$};
		\draw[color=black] (2.6,0.84) node {$AB$};
		\draw [fill=qqqqff] (1.,5.) circle (2.5pt);
		\draw[color=qqqqff] (1.14,5.36) node {$C$};
		\draw[color=black] (0.72,4.5) node {$AC$};
		\draw[color=qqwuqq] (1.3,0.68) node {$\ma BAC = 90\textrm{\degre}$};
		\draw [fill=xdxdff] (1.,4.) circle (2.5pt);
		\draw[color=xdxdff] (1.14,4.36) node {$D$};
		\draw[color=black] (0.72,2.82) node {$AD$};
		\draw [fill=qqqqff] (4.,4.) circle (2.5pt);
		\draw[color=qqqqff] (4.14,4.36) node {$E$};
		\draw[color=black] (4.22,2.56) node {$BE$};
		\draw[color=black] (2.54,4.32) node {$DE$};
		\end{scriptsize}
		\end{tikzpicture}
	\end{figure}
\end{proof}

\begin{proposition}\label{b1pr47}
	Given a right triangle $\triangle ABC$ where $BC$ subtends the right angle, the sum of the squares generated by $AB$ and $AC$ is equal to the square generated by $BC$.
\end{proposition}
\begin{proof}
	Since $BC$ subtends the right angle, we know $\ma BAC = 90$.
	By \autoref{b1pr46}, let $\square ACHK$, $\square ABFG$, and $\square BCED$ be the squares generated by $AC$, $AB$, and $BC$ respectively.
	Let $L$ on $DE$ such that $AL || BD$, thus $AL || CE$ because $\square BCED$ is a square.
	Draw $AD$ and $FC$.
	Since $\square BAGF$ is a square, we know $\ma BAG =90$.
	Thus, $\ma BAG + \ma BAC = 90 + 90=180$ and $GA$ lies on a straight line with $AC$ by Proposition 1.14.
	Similarly, $HA$ lies on a straight line with $AB$.
	Since $\square DBCE$ and $\square FBAG$ are squares, we know $\ma DBC=\ma FBA = 90$.
	Therefore, $\ma DBA=\ma DBC+\ma ABC=\ma FBA +\ma ABC = \ma FBC$ by Common Notion 1.2.
	Since $DB=BC$ and $FB=BA$ the sides $AB$, $BD$ are equal to $FB$, $BC$ respectively.
	Thus, $AD=BC$ and $\triangle ABD\cong \triangle FBC$ by Proposition 1.4 (SAS).
	Since $\triangle ABD$ and parallelogram $BL$ have the same base and are in the parallels, $BD$ and $BL$, $2\triangle ABD= BL$ by Proposition 1.41.
	Similarly $\square GB=2\triangle FBC$, and since $\triangle FBC = \triangle ABD$, $\square GB= BL$.
	Using a similar argument, we get parallelogram $CL=\square GB$ finally yielding $\square BDEC = \square GB +\square HC$.

	\begin{figure}[h!]
		\centering
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(0.5,0.5) rectangle (5.,4.5);
		\fill[color=zzttqq,fill=zzttqq,fill opacity=0.1] (2.,2.) -- (3.,2.) -- (2.,3.) -- cycle;
		\draw [color=zzttqq] (2.,2.)-- (3.,2.);
		\draw [color=zzttqq] (3.,2.)-- (2.,3.);
		\draw [color=zzttqq] (2.,3.)-- (2.,2.);
		\draw (2.,3.)-- (3.,4.);
		\draw (3.,4.)-- (4.,3.);
		\draw (3.,2.)-- (4.,3.);
		\draw (2.,2.)-- (2.,1.);
		\draw (3.,2.)-- (3.,1.);
		\draw (2.,1.)-- (3.,1.);
		\draw (2.,2.)-- (1.,2.);
		\draw (2.,3.)-- (1.,3.);
		\draw (1.,2.)-- (1.,3.);
		\draw (2.,3.)-- (3.,1.);
		\draw (2.,2.)-- (3.,4.);
		\draw (3.5,3.5)-- (2.,2.);
		\draw (2.,2.)-- (4.,3.);
		\begin{scriptsize}
		\draw [fill=qqqqff] (2.,2.) circle (2.5pt);
		\draw[color=qqqqff] (1.86,1.8666666666666687) node {$A$};
		\draw [fill=qqqqff] (3.,2.) circle (2.5pt);
		\draw[color=qqqqff] (3.06,2.16) node {$B$};
		\draw [fill=qqqqff] (2.,3.) circle (2.5pt);
		\draw[color=qqqqff] (2.064444444444442,3.155555555555557) node {$C$};
		\draw [fill=qqqqff] (3.,4.) circle (2.5pt);
		\draw[color=qqqqff] (3.06,4.16) node {$D$};
		\draw [fill=uuuuuu] (4.,3.) circle (1.5pt);
		\draw[color=uuuuuu] (4.064444444444439,3.12) node {$E$};
		\draw [fill=qqqqff] (2.,1.) circle (2.5pt);
		\draw[color=qqqqff] (2.064444444444442,1.155555555555558) node {$F$};
		\draw [fill=qqqqff] (3.,1.) circle (2.5pt);
		\draw[color=qqqqff] (3.06,1.155555555555558) node {$G$};
		\draw [fill=qqqqff] (1.,2.) circle (2.5pt);
		\draw[color=qqqqff] (1.06,2.16) node {$H$};
		\draw [fill=qqqqff] (1.,3.) circle (2.5pt);
		\draw[color=qqqqff] (1.06,3.155555555555557) node {$I$};
		\draw [fill=xdxdff] (3.5,3.5) circle (2.5pt);
		\draw[color=xdxdff] (3.566666666666663,3.662222222222223) node {$J$};
		\end{scriptsize}
		\end{tikzpicture}
	\end{figure}
\end{proof}

\begin{proposition}\label{b1pr48}
	Suppose $\triangle ABC$ satisfies the condition that the sum of the squares generated by two sides of the triangle equals the square generated by the third side.
	Then $\triangle ABC$ is right.
\end{proposition}
\begin{proof}
	Without loss of generality, suppose $AB^2 + AC^2=BC^2$.
	We want to show $\ma BAC = 90$.
	Let $AD$ be drawn from $A$ such that $\ma CAD = 90$ and $AD=BA$.
	Draw $DC$.
	Since $DA=AB$, the squares generated by $DA$ and $AB$ are congruent.
	Thus, the squares $DA+AC$ are equal to the squares $BA+AC$.
	However, the square on $DC$ equals the squares on $DA$ and $AC$ since $\ma DAC=90$ by \autoref{b1pr47}.
	Furthermore, the square on $BC$ is equal to the squares on $BA,AC$ by assumption.
	Thus, the square on $DC$ is equal to the square on $BC$.
	Moreover, because $DA=AB$ and $AC$ is common, the sides $DA,AC$ are equal to $BA,AC$.
	Additionally, $DC=BC$ and thus the angles $\ma DAC =\ma BAC=90$ and $\triangle ABC$ is right.
\end{proof}

\book

\setcounter{proposition}{13}
\begin{proposition}\label{b2pr14}
	We can construct a square with area equal to any given rectilineal figure.
\end{proposition}
\begin{proof}
	Let $A$ represent the given rectilineal figure.
	By Proposition 1.45 there exists a rectangle $BCDE$, which we denote $BD$ such that the area of $BD$ equals the area of $A$.
	To complete the proof we need to build a square whose area equals the area of $BD$.

	There are two cases to consider: the case where $BD=ED$ and the case where $BD\neq ED$.
	If $BD=ED$, then $BD$ is a square whose area equals the area of $A$, which completes the proof.
	Otherwise $BD\neq ED$ and without loss of generality we can assume $BD > ED$.

	By Proposition 1.3, we extend $BE$ to $BF$ with the condition that $EF=ED$.
	Using Proposition 1.10, we bisect $BF$ and call the point of bisection $G$.
	Let $\circ\, G$ be the circle of radius $BG=GF$ centered at $G$.
	Extend $ED$ until it intersects $\circ\, G$, call the point of intersection $H$.

	We know that $BF=BG+GF$ where $BG=GF$ and $BF=BE+EF$ where $BE\neq EF$.
	Let $J$ be a rectangle of side lengths $BE$ and $EF$.
	Then by Proposition 2.5, we know the area of $J$ plus the area of the square on $EG$ equals the area of the square on $GH$.
	However, $F$ and $H$ lie on the circumference of $\circ\, G$ and thus $GF=GH$.
	And thus,
	\[
		\area{J} +\area{\square EG} =\area{\square GH}.
	\]
	Furthermore $\triangle GEH$ is right and thus,
	\[
		\area{\square EH}+\area{\square EG}=\area{\square GH}
	\] by \autoref{b1pr47}.
	The previous and an application of Common Notion 1.1 yield
	\[
		\area{J} +\area{\square EG} = \area{\square EH}+\area{\square EG}
	\]
	or equivalently
	\[
		\area{J} = \area{\square EH}.
	\]
	However, since $ED=EF$, the rectangle $BD$ is a rectangle of side lengths $EF$ and $BE$.
	Thus,
	\[
		\area{A} = \area{BD} = \area{J} = \area{\square EH}
	\]
	which completes the proof.

	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(0.75,0.3) rectangle (6.25,3.75);
		\fill[color=zzttqq,fill=zzttqq,fill opacity=0.1] (1.,1.) -- (1.1776584379538713,2.041615772756342) -- (1.8550271540184036,2.639958138613346) -- (1.4598954029807596,1.3642470566918092) -- cycle;
		\fill[color=zzttqq,fill=zzttqq,fill opacity=0.1] (3.,2.) -- (3.,1.) -- (5.,1.) -- (5.,2.) -- cycle;
		\fill[color=zzttqq,fill=zzttqq,fill opacity=0.1] (4.5,2.) -- (5.,3.414213562373095) -- (5.,2.) -- cycle;
		\draw [color=zzttqq] (1.,1.)-- (1.1776584379538713,2.041615772756342);
		\draw [color=zzttqq] (1.1776584379538713,2.041615772756342)-- (1.8550271540184036,2.639958138613346);
		\draw [color=zzttqq] (1.8550271540184036,2.639958138613346)-- (1.4598954029807596,1.3642470566918092);
		\draw [color=zzttqq] (1.4598954029807596,1.3642470566918092)-- (1.,1.);
		\draw [color=zzttqq] (3.,2.)-- (3.,1.);
		\draw [color=zzttqq] (3.,1.)-- (5.,1.);
		\draw [color=zzttqq] (5.,1.)-- (5.,2.);
		\draw [color=zzttqq] (5.,2.)-- (3.,2.);
		\draw (5.,2.)-- (6.,2.);
		\draw(4.5,2.) circle (1.5cm);
		\draw [color=zzttqq] (4.5,2.)-- (5.,3.414213562373095);
		\draw [color=zzttqq] (5.,3.414213562373095)-- (5.,2.);
		\draw [color=zzttqq] (5.,2.)-- (4.5,2.);
		\begin{scriptsize}
		\draw[color=zzttqq] (1.4034480099753819,1.8609841151391333) node {$A$};
		\draw [fill=qqqqff] (3.,2.) circle (2.5pt);
		\draw[color=qqqqff] (3.0742908429345612,2.1996684731714) node {$B$};
		\draw [fill=qqqqff] (3.,1.) circle (2.5pt);
		\draw[color=qqqqff] (2.848501270913051,1.115878527468147) node {$C$};
		\draw [fill=qqqqff] (5.,1.) circle (2.5pt);
		\draw[color=qqqqff] (5.083818033926008,1.2061943562767514) node {$D$};
		\draw [fill=qqqqff] (5.,2.) circle (2.5pt);
		\draw[color=qqqqff] (5.083818033926008,2.1996684731714) node {$E$};
		\draw [fill=qqqqff] (6.,2.) circle (2.5pt);
		\draw[color=qqqqff] (6.077292150820654,2.1996684731714) node {$F$};
		\draw [fill=xdxdff] (4.5,2.) circle (2.5pt);
		\draw[color=xdxdff] (4.372580882058249,2.2561158661767777) node {$G$};
		\draw [fill=uuuuuu] (5.,3.414213562373095) circle (1.5pt);
		\draw[color=uuuuuu] (5.083818033926008,3.576984862502617) node {$H$};
		\end{scriptsize}
		\end{tikzpicture}
	\end{figure}
\end{proof}

\book

\setcounter{proposition}{35}
\begin{proposition}\label{b3pr36}
	Let $K$ be a circle and let $D$ be a point outside of $K$.
	Moreover, let $L$ and $M$ be straight lines through $D$ such that $L$ cuts $K$ and $M$ lies tangent to $K$.
	Furthermore, let $A$ be the point where $L$ enters $K$, let $C$ be the point where $L$ exits $K$, and let $B$ be the point where $M$ touches $K$.
	Then any rectangle with side lengths $DC$ and $DA$ has area equal to the square on $DB$.
\end{proposition}

\begin{proposition}\label{b3pr37}
	If a point is taken outside a circle and from the point there fall on the circle two straight lines, if one of them cuts the circle, and the other falls on it, and if further the rectangle contained by the whole of the straight line which cuts the circle and the straight line intercepted on it outside between the point and the convex circumference equals the square on the straight line which falls on the circle, then the straight line which falls on it touches the circle.
\end{proposition}

\end{document}
